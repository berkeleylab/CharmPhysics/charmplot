#!/usr/bin/env python
from charmplot.common import www
from charmplot.control import globalConfig
from charmplot.control import tools

from multiprocessing import Pool
import logging
import os
import ROOT
import sys
import optparse  # for the user’s old-style OptionParser

# ATLAS Style
dirname = os.path.join(os.path.dirname(__file__), "../../atlasrootstyle")
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.LoadMacro(os.path.join(dirname, "AtlasStyle.C"))
ROOT.SetAtlasStyle()

# Set up logging
root_logger = logging.getLogger()
root_logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root_logger.addHandler(handler)


def modify_sample_name(sample_name, channel):
    """
    You had commented-out logic in the original code. Preserved here.
    """
    # if "WplusD_Matched" in sample_name:
    #     if "minus" in channel.name:
    #         sample_name += "_minus"
    #     elif "plus" in channel.name:
    #         sample_name += "_plus"
    return sample_name


def get_err_hist(f, par, variation, default):
    """
    Retrieve an Up/Down histogram from the file 'f'. If not found, fall back to 'default'.
    """
    h_err = None
    name = default.split("postFit")[0]
    h_temp = f.Get(f"{name}{par}_{variation}_postFit")
    if h_temp:
        h_err = h_temp.Clone(f"{h_temp.GetName()}_err_{variation}")
    else:
        # logging.warning(...)
        h_mc_default = f.Get(default)
        if h_mc_default:
            h_err = h_mc_default.Clone(f"{h_mc_default.GetName()}_err_{variation}")
    return h_err


def process_one_plot(
    plot,
    var,
    options,
    conf,
    corr_parameters,
    result,
    trex_histogram_folder
):
    """
    Process a single 'plot' entry in parallel.

    Writes:
      - A dedicated PDF: post_fit/{conf.out_name}/{channel_name}_{var.name}.pdf
      - A dedicated ROOT file: post_fit/{conf.out_name}/{channel_name}_{var.name}.root
    """
    import math
    import ROOT
    import logging
    import os
    import re
    from charmplot.control.channel import Channel
    from charmplot.common import utils

    logger = logging.getLogger("worker")

    # Because we are working on exactly one 'plot' dictionary at a time:
    #  e.g. plot is {'+': [ch1, ch2,...], '-': [ch3,...]}
    # We replicate the logic that was originally inside the loop.

    # Skip if empty
    if not plot['+']:
        logger.info("No positive channels, skipping.")
        return None

    # We'll get the "channel_temp", build the combined channel name, etc.
    channel_temp = plot['+'][0]
    channels_all = plot['+'] + plot['-']
    channel_name = "_".join([channel.name for channel in channels_all])

    # Check if inclusive, minus, plus, etc. (unchanged logic from your code)
    minus = False
    plus = False
    inclusive = False
    inclusive_minus = False
    inclusive_plus = False
    has_OS = False
    has_SS = False
    tags = []
    tag = ""

    if len(channels_all) > 2:
        for c in channels_all:
            if "minus" in c.name:
                minus = True
            elif "plus" in c.name.replace("Dplus", ""):
                plus = True
            if "OS" in c.name:
                has_OS = True
            elif "SS" in c.name:
                has_SS = True
            tmp = re.findall("[0-9]tag", c.name)
            if tmp:
                tags += tmp
        if (minus and plus) or (not minus and not plus):
            inclusive = True
        elif minus and not plus:
            inclusive_minus = True
        elif plus and not minus:
            inclusive_plus = True

    if len(tags) > 0:
        # If all tags are the same, pick it
        if tags.count(tags[0]) == len(tags):
            tag = tags[0]

    # Prepare final label(s)
    labels = []
    for label in channel_temp.label:
        if inclusive and "channel" in label:
            label = "W^{#pm} channel"
        if len(channels_all) > 2 and "bin" in label:
            if inclusive_minus:
                label = "W^{-} inclusive"
            elif inclusive_plus:
                label = "W^{+} inclusive"
        if len(plot['-']) > 0:
            label = label.replace("OS", "OS-SS")
        if len(plot['+']) > 1 and "tag" in label:
            label = label.split(",")[0]
        labels.append(label)
    if tag and (inclusive or inclusive_minus or inclusive_plus):
        labels[-1] += f", {tag}"
    labels[-1] += ', post-fit'

    tag_name = ""
    if tag:
        tag_name = f"{tag}_"
    if inclusive:
        channel_name = f"{tag_name}inclusive"
    elif inclusive_minus:
        channel_name = f"{tag_name}inclusive_minus"
    elif inclusive_plus:
        channel_name = f"{tag_name}inclusive_plus"

    if has_OS and not has_SS:
        channel_name = "OS_" + channel_name
    elif has_SS and not has_OS:
        channel_name = "SS_" + channel_name
    elif has_OS and has_SS:
        channel_name = "OS_SS_" + channel_name

    chan = Channel(channel_name, labels, channel_temp.lumi, [], [])

    # Open all required postFit files for each channel
    file_suffix = ""
    if var.name not in ["Dmeson_m", "Dmeson_mdiff", "Dmeson_m_fit", "Dmeson_mdiff_fit"]:
        file_suffix = f"_{var.name}"
    files = {}
    for ch in channels_all:
        file_name = f"{ch.name}{file_suffix}_postFit.root"
        full_path = os.path.join(trex_histogram_folder, file_name)
        if os.path.isfile(full_path):
            files[ch] = ROOT.TFile(full_path, "READ")
        else:
            logger.warning(f"File {full_path} not found. This channel will be missing.")

    # Build a list of samples that appear in any channel
    sample_names = []
    samples = []
    for ch in channels_all:
        for sname in ch.samples:
            sample = conf.get_sample(sname)
            if sample.shortName not in sample_names:
                sample_names.append(sample.shortName)
                samples.append(sample)

    # Merge MC for each sample
    h_bkg = None
    h_sig = None
    mc_map = {}
    for sample in samples:
        sample_name = sample.shortName
        h_sum = None

        # Add histograms from positive channels
        for ch in plot['+']:
            if ch not in files:
                continue
            h_temp = files[ch].Get(f"h_{modify_sample_name(sample_name, ch)}_postFit")
            if h_temp:
                if h_sum is None:
                    h_sum = h_temp.Clone(f"{h_temp.GetName()}_{chan.name}")
                else:
                    h_sum.Add(h_temp)
                if options.subtract_background:
                    if modify_sample_name(sample_name, ch) != options.signal:
                        if h_bkg is None:
                            h_bkg = h_temp.Clone(f"{chan.name}_bkg")
                        else:
                            h_bkg.Add(h_temp)
                    else:
                        if h_sig is None:
                            h_sig = h_temp.Clone(f"{chan.name}_sig")
                        else:
                            h_sig.Add(h_temp)

        # Subtract histograms from negative channels (e.g. SS)
        for ch in plot['-']:
            if ch not in files:
                continue
            h_temp = files[ch].Get(f"h_{modify_sample_name(sample_name, ch)}_postFit")
            if h_temp:
                if h_sum is None:
                    h_sum = h_temp.Clone(f"{h_temp.GetName()}_{chan.name}")
                    h_sum.Scale(-1.0)
                else:
                    h_sum.Add(h_temp, -1.0)
                if options.subtract_background:
                    if modify_sample_name(sample_name, ch) != options.signal:
                        if h_bkg is None:
                            h_bkg = h_temp.Clone(f"{chan.name}_bkg")
                            h_bkg.Scale(-1.)
                        else:
                            h_bkg.Add(h_temp, -1.)
                    else:
                        if h_sig is None:
                            h_sig = h_temp.Clone(f"{chan.name}_sig")
                            h_sig.Scale(-1.)
                        else:
                            h_sig.Add(h_temp, -1.)

        # Keep only if non-empty
        if h_sum and abs(h_sum.GetSum()) > 1e-2:
            # If subtracting background, keep only signal
            if (options.subtract_background and modify_sample_name(sample_name, ch) == options.signal) or not options.subtract_background:
                mc_map[sample] = h_sum

    if not mc_map:
        logger.warning(f"No MC found for combined channel {chan.name}. Skipping.")
        # Close any input files
        for f in files.values():
            f.Close()
        return None

    # Build 'data' histogram
    h_data = None
    if plot['+']:
        for ch in plot['+']:
            if ch not in files:
                continue
            h_temp = files[ch].Get("h_Data")
            if not h_temp:
                continue
            if h_data is None:
                h_data = h_temp.Clone(f"{h_temp.GetName()}_{chan.name}")
            else:
                h_data.Add(h_temp)
            # If there's a negative channel partner, subtract it
            if len(plot['-']):
                for ch_ss in files:
                    # see if it’s the corresponding SS channel
                    if ch_ss.name == ch.name.replace("OS_", "SS_"):
                        h_temp_SS = files[ch_ss].Get("h_Data")
                        if h_temp_SS:
                            h_data.Add(h_temp_SS, -1.0)
        if options.subtract_background and h_bkg:
            h_data.Add(h_bkg, -1.)

    # Build 'total MC' histogram
    h_mc_tot = None
    if plot['+']:
        for ch in plot['+']:
            if ch not in files:
                continue
            h_temp = files[ch].Get("h_tot_postFit")
            if not h_temp:
                continue
            if h_mc_tot is None:
                h_mc_tot = h_temp.Clone(f"{h_temp.GetName()}_{chan.name}")
            else:
                h_mc_tot.Add(h_temp)
            # If there's a negative channel partner, subtract it
            if len(plot['-']):
                for ch_ss in files:
                    if ch_ss.name == ch.name.replace("OS_", "SS_"):
                        h_temp_SS = files[ch_ss].Get("h_tot_postFit")
                        if h_temp_SS:
                            h_mc_tot.Add(h_temp_SS, -1.0)
        if options.subtract_background and h_bkg:
            # Keep only the 'signal' content in h_mc_tot
            for i in range(1, h_mc_tot.GetNbinsX() + 1):
                h_mc_tot.SetBinContent(i, h_sig.GetBinContent(i))
                h_mc_tot.SetBinError(i, h_sig.GetBinError(i))

    # Make the TCanvas
    canv = utils.make_canvas(h_data, var, chan, x=var.canvas_size[0], y=var.canvas_size[1])

    # Configure histograms
    canv.configure_histograms(mc_map, h_data, style=conf.style)

    # Make stack
    hs = utils.make_stack(samples, mc_map)

    # Stat error band
    g_mc_tot_err, g_mc_tot_err_only = utils.make_stat_err(h_mc_tot)

    # Systematic variations
    h_mc_tot_err_histograms_Up = []
    h_mc_tot_err_histograms_Dn = []
    for par in corr_parameters:
        h_mc_tot_Up = None
        h_mc_tot_Dn = None
        # Positive channels
        for ch in plot['+']:
            if ch not in files:
                continue
            h_temp_up = get_err_hist(files[ch], par, "Up", "h_tot_postFit")
            h_temp_dn = get_err_hist(files[ch], par, "Down", "h_tot_postFit")
            if h_temp_up:
                if not h_mc_tot_Up:
                    h_mc_tot_Up = h_temp_up.Clone(f"{h_temp_up.GetName()}_{chan.name}_err_up")
                else:
                    h_mc_tot_Up.Add(h_temp_up)
            if h_temp_dn:
                if not h_mc_tot_Dn:
                    h_mc_tot_Dn = h_temp_dn.Clone(f"{h_temp_dn.GetName()}_{chan.name}_err_dn")
                else:
                    h_mc_tot_Dn.Add(h_temp_dn)
        # Negative channels
        for ch in plot['-']:
            if ch not in files:
                continue
            h_temp_up = get_err_hist(files[ch], par, "Up", "h_tot_postFit")
            h_temp_dn = get_err_hist(files[ch], par, "Down", "h_tot_postFit")
            if h_temp_up:
                if not h_mc_tot_Up:
                    h_mc_tot_Up = h_temp_up.Clone(f"{h_temp_up.GetName()}_{chan.name}_err_up")
                    h_mc_tot_Up.Scale(-1.)
                else:
                    h_mc_tot_Up.Add(h_temp_up, -1.)
            if h_temp_dn:
                if not h_mc_tot_Dn:
                    h_mc_tot_Dn = h_temp_dn.Clone(f"{h_temp_dn.GetName()}_{chan.name}_err_dn")
                    h_mc_tot_Dn.Scale(-1.)
                else:
                    h_mc_tot_Dn.Add(h_temp_dn, -1.)

        # Subtract nominal if subtracting background
        if h_mc_tot_Up and h_mc_tot_Dn:
            if options.subtract_background and h_bkg:
                for i in range(1, h_mc_tot_Up.GetNbinsX() + 1):
                    h_mc_tot_Up.SetBinContent(i, h_mc_tot_Up.GetBinContent(i) - h_bkg.GetBinContent(i))
                    h_mc_tot_Dn.SetBinContent(i, h_mc_tot_Dn.GetBinContent(i) - h_bkg.GetBinContent(i))
            # difference from nominal
            h_mc_tot_Up.Add(h_mc_tot, -1.)
            h_mc_tot_Dn.Add(h_mc_tot, -1.)
        h_mc_tot_err_histograms_Up.append(h_mc_tot_Up)
        h_mc_tot_err_histograms_Dn.append(h_mc_tot_Dn)

    # Combine stat + syst errors with correlations
    n_pars = len(corr_parameters)
    for xbin in range(1, h_mc_tot.GetNbinsX() + 1):
        # Start with stat error in quadrature
        # The g_mc_tot_err array indices are zero-based
        if g_mc_tot_err.GetEYhigh()[xbin - 1] != 0.0:
            g_mc_tot_err.GetEYhigh()[xbin - 1] = g_mc_tot_err.GetEYhigh()[xbin - 1]**2
        if g_mc_tot_err.GetEYlow()[xbin - 1] != 0.0:
            g_mc_tot_err.GetEYlow()[xbin - 1] = g_mc_tot_err.GetEYlow()[xbin - 1]**2

        # Off-diagonal terms
        for i in range(n_pars):
            for j in range(i):
                up_i = h_mc_tot_err_histograms_Up[i]
                dn_i = h_mc_tot_err_histograms_Dn[i]
                up_j = h_mc_tot_err_histograms_Up[j]
                dn_j = h_mc_tot_err_histograms_Dn[j]
                if up_i and dn_i and up_j and dn_j:
                    par1 = corr_parameters[i]
                    par2 = corr_parameters[j]
                    # The original code used 'alpha_' or 'gamma_' for certain parameters
                    if "SymmBkg" not in par1 and "mu_" not in par1 and not par1.startswith("stat_"):
                        par1 = "alpha_" + par1
                    elif "SymmBkg" in par1 or par1.startswith("stat_"):
                        par1 = "gamma_" + par1
                    if "SymmBkg" not in par2 and "mu_" not in par2 and not par2.startswith("stat_"):
                        par2 = "alpha_" + par2
                    elif "SymmBkg" in par2 or par2.startswith("stat_"):
                        par2 = "gamma_" + par2
                    corr = result.correlation(par1, par2)

                    err_up_i = up_i.GetBinContent(xbin)
                    err_up_j = up_j.GetBinContent(xbin)
                    err_dn_i = dn_i.GetBinContent(xbin)
                    err_dn_j = dn_j.GetBinContent(xbin)

                    # Off-diagonal contribution
                    err_up = err_up_i * err_up_j * corr * 2
                    err_dn = err_dn_i * err_dn_j * corr * 2

                    g_mc_tot_err.GetEYhigh()[xbin - 1] += err_up
                    g_mc_tot_err.GetEYlow()[xbin - 1] += err_dn

        # Diagonal terms
        for i in range(n_pars):
            up_i = h_mc_tot_err_histograms_Up[i]
            dn_i = h_mc_tot_err_histograms_Dn[i]
            if up_i and dn_i:
                err_up_i = up_i.GetBinContent(xbin)
                err_dn_i = dn_i.GetBinContent(xbin)
                err_up = err_up_i**2
                err_dn = err_dn_i**2
                g_mc_tot_err.GetEYhigh()[xbin - 1] += err_up
                g_mc_tot_err.GetEYlow()[xbin - 1] += err_dn

        # Final sqrt
        if g_mc_tot_err.GetEYhigh()[xbin - 1] < 0:
            g_mc_tot_err.GetEYhigh()[xbin - 1] = 0
        if g_mc_tot_err.GetEYlow()[xbin - 1] < 0:
            g_mc_tot_err.GetEYlow()[xbin - 1] = 0
        g_mc_tot_err.GetEYhigh()[xbin - 1] = math.sqrt(g_mc_tot_err.GetEYhigh()[xbin - 1])
        g_mc_tot_err.GetEYlow()[xbin - 1] = math.sqrt(g_mc_tot_err.GetEYlow()[xbin - 1])

        # Make ratio error
        if h_mc_tot.GetBinContent(xbin) > 0:
            g_mc_tot_err_only.GetEYhigh()[xbin - 1] = g_mc_tot_err.GetEYhigh()[xbin - 1] / h_mc_tot.GetBinContent(xbin)
            g_mc_tot_err_only.GetEYlow()[xbin - 1] = g_mc_tot_err.GetEYlow()[xbin - 1] / h_mc_tot.GetBinContent(xbin)
        else:
            g_mc_tot_err_only.GetEYhigh()[xbin - 1] = 0
            g_mc_tot_err_only.GetEYlow()[xbin - 1] = 0

    # Make ratio
    h_ratio = utils.make_ratio(h_data, h_mc_tot)
    gr_data = utils.get_gr_from_hist(h_data)
    gr_ratio = utils.get_gr_from_hist(h_ratio)

    # Top pad
    canv.pad1.cd()
    canv.make_legend(h_data, h_mc_tot, mc_map, samples,
                     print_yields=(not options.paper),
                     show_error=False)

    # Optionally normalize bins to unity
    if var.per_unit:
        utils.normalize_to_unit(hs, hists=[h_mc_tot, h_data], grs=[g_mc_tot_err, gr_data])

    hs.Draw("same hist")
    h_mc_tot.Draw("same hist")
    g_mc_tot_err.Draw("e2")
    gr_data.Draw("pe0")

    # Rescale maximum
    canv.set_maximum((h_data, h_mc_tot), var, mc_min=utils.get_mc_min(mc_map, samples))
    if options.y_axis_range:
        canv.proxy_up.SetMaximum(float(options.y_axis_range))

    # Look for negative bin content if not for a publication
    if not options.paper:
        min_negative = {}
        for s in samples:
            if "Offset" not in s.name and "Mock" not in s.name:
                if s not in mc_map:
                    continue
                h_test = mc_map[s]
                for i in range(1, h_test.GetNbinsX() + 1):
                    if h_test.GetBinContent(i) < 0:
                        min_negative[i] = min_negative.get(i, 0) + h_test.GetBinContent(i)
        if min_negative:
            val_min = min(min_negative.values())
            canv.proxy_up.SetMinimum(val_min)

    ROOT.gPad.RedrawAxis()

    # Bottom pad
    canv.pad2.cd()
    xmin = g_mc_tot_err_only.GetX()[0] - g_mc_tot_err_only.GetEXlow()[0]
    xmax = g_mc_tot_err_only.GetX()[g_mc_tot_err_only.GetN() - 1] + g_mc_tot_err_only.GetEXhigh()[g_mc_tot_err_only.GetN() - 1]
    unity_line = ROOT.TLine(xmin, 1.0, xmax, 1.0)
    unity_line.SetLineStyle(1)
    unity_line.Draw()
    g_mc_tot_err_only.Draw("e2")
    gr_ratio.Draw("pe0")

    # Ratio range
    ratio_low = 1.01 - float(options.ratio_range) / 100.
    ratio_high = 0.99 + float(options.ratio_range) / 100.
    canv.set_ratio_range(ratio_low, ratio_high, override=True)
    ROOT.gPad.RedrawAxis()

    # Save PDF
    pdf_filename = f"post_fit/{conf.out_name}/{chan.name}_{var.name}.pdf"
    canv.print(pdf_filename)

    # Write to a unique output ROOT file
    root_filename = f"post_fit/{conf.out_name}/{chan.name}_{var.name}.root"
    outfile = ROOT.TFile(root_filename, "RECREATE")
    outfile.cd()
    if gr_data:
        gr_data.Write(f"{chan.name}_{var.name}_data")
    if h_mc_tot:
        h_mc_tot.Write(f"{chan.name}_{var.name}_mc_tot")
    for s in mc_map:
        mc_map[s].Write(f"{chan.name}_{var.name}_{s.shortName}")
    outfile.Close()

    # Close all input files
    for f in files.values():
        f.Close()

    logger.info(f"Finished processing combined channel {chan.name}")
    return pdf_filename


def main(options, conf):
    """
    Main function: discovers channels, builds the 'plots' list, spawns parallel
    workers via starmap, then optionally merges results or stages them.
    """
    # Prepare folder with postFit ROOT files
    trex_histogram_folder = os.path.join(options.trex_input, "Histograms")

    # Channels
    channels = set()

    # Fitted variable
    var = conf.get_var(options.var)
    logging.info(f"Got variable {var}")

    # Discover which channels exist by scanning the folder
    for file in os.listdir(trex_histogram_folder):
        # Skip if the file is for Dmeson but var is not in a known set
        if var.name in ["Dmeson_m", "Dmeson_mdiff", "Dmeson_m_fit", "Dmeson_mdiff_fit"]:
            if "Dmeson" in file:
                continue
        if file.endswith("postFit.root"):
            channel_name = file.replace("_postFit.root", "")
            if "Dmeson" in channel_name:
                channel_name = channel_name.split("_Dmeson")[0]
            logging.info(f"Searching for channel {channel_name}...")
            channel = conf.get_channel(channel_name)
            if not channel:
                logging.warning(f"Channel not found for string {channel_name}")
            else:
                logging.info(f"Found channel {channel_name}")
                if options.skip_channel:
                    keep = True
                    for skip in options.skip_channel.split(","):
                        if skip in channel.name:
                            keep = False
                            break
                    if not keep:
                        logging.info(f"Skipping channel {channel.name}..")
                        continue
                channels.add(channel)

    # Build sets of channels and define the final list 'plots'
    individual_plots = []
    OS_minus_SS_plots = []
    OS_minus_SS_total = {'+': [], '-': []}
    OS_total = {'+': [], '-': []}
    SS_total = {'+': [], '-': []}
    OS_minus_SS_total_1tag = {'+': [], '-': []}
    OS_total_1tag = {'+': [], '-': []}
    SS_total_1tag = {'+': [], '-': []}
    OS_minus_SS_total_2tag = {'+': [], '-': []}
    OS_total_2tag = {'+': [], '-': []}
    SS_total_2tag = {'+': [], '-': []}
    OS_minus_SS_total_minus = {'+': [], '-': []}
    OS_minus_SS_total_plus = {'+': [], '-': []}

    # Fill 'individual_plots'
    for ch in channels:
        individual_plots.append({'+': [ch], '-': []})
        logging.info(f"Added channel {ch.name}..")

    # Fill OS_minus_SS_plots
    for channel_OS in channels:
        if "OS_" not in channel_OS.name:
            continue
        for channel_SS in channels:
            if channel_SS.name == channel_OS.name.replace("OS_", "SS_"):
                OS_minus_SS_plots.append({'+': [channel_OS], '-': [channel_SS]})
                if "0tag" in channel_SS.name:
                    OS_minus_SS_total['+'].append(channel_OS)
                    OS_minus_SS_total['-'].append(channel_SS)
                    OS_total['+'].append(channel_OS)
                    SS_total['+'].append(channel_SS)
                    if "minus" in channel_SS.name:
                        OS_minus_SS_total_minus['+'].append(channel_OS)
                        OS_minus_SS_total_minus['-'].append(channel_SS)
                    elif "plus" in channel_SS.name:
                        OS_minus_SS_total_plus['+'].append(channel_OS)
                        OS_minus_SS_total_plus['-'].append(channel_SS)
                elif "1tag" in channel_SS.name:
                    OS_minus_SS_total_1tag['+'].append(channel_OS)
                    OS_minus_SS_total_1tag['-'].append(channel_SS)
                    OS_total_1tag['+'].append(channel_OS)
                    SS_total_1tag['+'].append(channel_SS)
                elif "2tag" in channel_SS.name:
                    OS_minus_SS_total_2tag['+'].append(channel_OS)
                    OS_minus_SS_total_2tag['-'].append(channel_SS)
                    OS_total_2tag['+'].append(channel_OS)
                    SS_total_2tag['+'].append(channel_SS)
                break

    # Load correlation matrix
    logging.info("Loading correlation matrix...")
    corr_dict = tools.parse_yaml_file(os.path.join(options.trex_input, "CorrelationMatrix.yaml"))
    res_file = ROOT.TFile(os.path.join(options.trex_input, "Fits", os.path.basename(options.trex_input) + ".root"), "READ")
    result = res_file.Get("nll_simPdf_newasimovData_with_constr")
    assert result, "RooFitResult not found in the fit file"
    corr_parameters = None
    for x in corr_dict:
        if 'parameters' in x:
            corr_parameters = x['parameters']

    # Construct final 'plots' list (same as your code)
    plots = OS_minus_SS_plots
    plots += [OS_total, SS_total, OS_minus_SS_total]
    plots += [OS_total_1tag, SS_total_1tag, OS_minus_SS_total_1tag]
    plots += [OS_total_2tag, SS_total_2tag, OS_minus_SS_total_2tag]
    if var.name in ["Dmeson_m", "Dmeson_mdiff", "Dmeson_m_fit", "Dmeson_mdiff_fit"]:
        plots += individual_plots
    if var.name in ["Dmeson_track_jet_10_zt_unfold", "Dmeson_track_jet_10_zl_unfold",
                    "Dmeson_track_jet_10_pt_rel_unfold", "Dmeson_track_jet_10_pt",
                    "Dmeson_pt"]:
        plots = [OS_total, SS_total, OS_minus_SS_total,
                 OS_minus_SS_total_1tag, OS_minus_SS_total_2tag]

    # Instead of a serial for-loop, we use a Pool:
    n_threads = int(options.threads)
    logging.info(f"Processing {len(plots)} plots in parallel using {n_threads} threads...")
    with Pool(processes=n_threads) as pool:
        # build the argument list:
        args_list = [
            (p, var, options, conf, corr_parameters, result, trex_histogram_folder)
            for p in plots
        ]
        pool.starmap(process_one_plot, args_list)

    # Close the read-only file with correlation matrix
    res_file.Close()


if __name__ == "__main__":
    parser = optparse.OptionParser()

    parser.add_option('-a', '--analysis-config',
                      action="store", dest="analysis_config",
                      help="analysis config file")
    parser.add_option('-b', '--subtract-background',
                      action="store_true", dest="subtract_background",
                      help="subtract background from data and total MC")
    parser.add_option('-s', '--signal',
                      action="store", dest="signal",
                      default="MGPy8EG_FxFx_WplusD_Matched",
                      help="name of the signal sample (to be used with '-b')")
    parser.add_option('-v', '--var',
                      action="store", dest="var",
                      help="fitted variable",
                      default="Dmeson_m")
    parser.add_option('-r', '--ratio-range',
                      action="store", dest="ratio_range",
                      default=50,
                      help="range of the ratio y-axis")
    parser.add_option('-k', '--skip-channel',
                      action="store", dest="skip_channel",
                      default="",
                      help="skip channels that include this in the name")
    parser.add_option('--suffix',
                      action="store", dest="suffix",
                      help="suffix for the output name")
    parser.add_option('--stage-out',
                      action="store_true", dest="stage_out",
                      help="copy plots to the www folder")
    parser.add_option('--trex-input',
                      action="store", dest="trex_input",
                      help="import post-fit trex plots")
    parser.add_option('--paper',
                      action="store_true", dest="paper")
    parser.add_option('-y', '--y-axis-range',
                      action="store", dest="y_axis_range")
    parser.add_option('-t', '--threads',
                      action="store", dest="threads",
                      help="number of threads",
                      default=8)

    options, args = parser.parse_args()

    # analysis configs
    config = options.analysis_config

    # output name (same logic)
    out_name = config.replace(".yaml", "").replace(".yml", "")
    if options.suffix:
        out_name = out_name.split("/")
        if out_name[0] != ".":
            out_name[0] += "_" + options.suffix
            out_name = "/".join(out_name)
        else:
            out_name[1] += "_" + options.suffix
            out_name = "/".join(out_name)

    # config object
    conf = globalConfig.GlobalConfig(config, out_name)

    # Make output folder
    if not os.path.isdir(os.path.join("post_fit", out_name)):
        os.makedirs(os.path.join("post_fit", out_name))

    # Run main
    main(options, conf)

    # Stage out to www folder if requested
    if options.stage_out:
        www.stage_out_plots(conf.out_name, conf.get_variables(), x=300, y=300)
