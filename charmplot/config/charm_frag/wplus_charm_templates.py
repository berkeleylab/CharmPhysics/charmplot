import ROOT
import sys

species = ["411", "413", "421", "431", "Baryon"]
files = {}

# Check if a jet radius parameter was provided
if len(sys.argv) != 2:
    print("Usage: python script.py <jet_radius>")
    sys.exit(1)

# Read the jet radius from the command line argument
jet_radius = sys.argv[1]

# jet collection
jet_collection = f"track_jet_{jet_radius}"

# Open the input ROOT files
for s in species:
    files[s] = ROOT.TFile(f"charm_frag_{s}MisMatched/charm_frag_templates/histograms.root", "READ")

# Create output file to save scaled histograms
output_file = ROOT.TFile("Sh_Wjets_scaled.root", "RECREATE")

# Function to scale Sherpa histograms to Madgraph


def scale_histogram(mg_hist, sh_hist):
    if mg_hist.Integral() == 0 or sh_hist.Integral() == 0:
        print(f"Warning: Zero integral for one of the histograms, skipping scaling.")
        return None

    scale_factor = mg_hist.Integral() / sh_hist.Integral()
    sh_hist_scaled = sh_hist.Clone()
    sh_hist_scaled.Scale(scale_factor)

    return sh_hist_scaled


# Variables and regions to process
variables = [f"Dmeson_{jet_collection}_zt_unfold", f"Dmeson_{jet_collection}_zl_unfold", "Dmeson_m_fit"]
regions = ["PR", "LS", "RS"]

# Initialize dictionaries to hold summed histograms for each variable, region, jet bin, and sign
summed_histograms = {
    region: {
        var: {
            "OS": {jet: None for jet in range(4)},
            "SS": {jet: None for jet in range(4)}
        }
        for var in variables
    }
    for region in regions
}

# Loop over species, regions, jet bins, and variables; scale OS and SS histograms separately, and sum them
for s in species:
    for region in regions:
        for jet in range(4):
            for var in variables:
                # Get Madgraph and Sherpa histograms for OS and SS
                hmgos = files[s].Get(f"MGPy8EG_FxFx_Wjets_{s}MisMatched_OS_0tag_Dplus_{region}_jet_bin_{jet}_{var}")
                hmgss = files[s].Get(f"MGPy8EG_FxFx_Wjets_{s}MisMatched_SS_0tag_Dplus_{region}_jet_bin_{jet}_{var}")
                hsgos = files[s].Get(f"Sherpa2211_Wjets_{s}MisMatched_Loose_OS_0tag_Dplus_{region}_jet_bin_{jet}_{var}")
                hsgss = files[s].Get(f"Sherpa2211_Wjets_{s}MisMatched_Loose_SS_0tag_Dplus_{region}_jet_bin_{jet}_{var}")

                # Scale Sherpa OS and SS histograms
                scaled_hsgos = scale_histogram(hmgos, hsgos) if hmgos and hsgos else None
                scaled_hsgss = scale_histogram(hmgss, hsgss) if hmgss and hsgss else None

                # Sum OS histograms across species
                if scaled_hsgos:
                    if not summed_histograms[region][var]["OS"][jet]:
                        summed_histograms[region][var]["OS"][jet] = scaled_hsgos.Clone(
                            f"SR_0tag_Dplus_OS_{region}_jet_bin_{jet}__{var.replace('_fit', '')}")
                    else:
                        summed_histograms[region][var]["OS"][jet].Add(scaled_hsgos)

                # Sum SS histograms across species
                if scaled_hsgss:
                    if not summed_histograms[region][var]["SS"][jet]:
                        summed_histograms[region][var]["SS"][jet] = scaled_hsgss.Clone(
                            f"SR_0tag_Dplus_SS_{region}_jet_bin_{jet}__{var.replace('_fit', '')}")
                    else:
                        summed_histograms[region][var]["SS"][jet].Add(scaled_hsgss)

# Save the summed histograms to the output file
for region in regions:
    for var in variables:
        for jet in range(4):
            if summed_histograms[region][var]["OS"][jet]:
                summed_histograms[region][var]["OS"][jet].Write()
            if summed_histograms[region][var]["SS"][jet]:
                summed_histograms[region][var]["SS"][jet].Write()

# Close output file
output_file.Close()

# Close all input files
for s in species:
    files[s].Close()
