from collections import defaultdict
from copy import deepcopy
import os
import ROOT


def main(options):

    SAMPLES = [
        "h_DibosonZjets_FxFx",
        "h_MGPy8EG_FxFx_Wjets_Charm",
        "h_MGPy8EG_FxFx_Wjets_MisMatched",
        "h_MGPy8EG_FxFx_WplusD_Matched",
        "h_MGPy8EG_FxFx_WplusD_MatchedNoFid",
        "h_Multijet_MatrixMethod",
        "h_Top_Matched",
        "h_Top_Charm",
        "h_Top_Rest",
        "h_tot",
    ]
    if options.meson == "Dplus":
        SAMPLES += ["h_MGPy8EG_FxFx_Wjets_Rest"]
    elif options.meson == "Dstar":
        SAMPLES += ["h_Sherpa2211_Wjets_Rest"]

    # find postfit files
    for reg in ["PR", "RS"]:
        for var in ["zt", "zl", "pt_rel"]:
            files = {charge: defaultdict(list) for charge in ["OS", "SS"]}
            for f in os.listdir(options.input):
                if f"Dmeson_track_jet_{options.radius}_{var}_unfold_postFit" in f and f"_{reg}_" in f and f.endswith(".root") and "ALL" not in f:
                    charge = f.split("_")[0]
                    tag = f.split("_")[1]
                    files[charge][tag].append(f)

            for charge in files:
                for tag in files[charge]:
                    out_name = os.path.join(options.input, f"{charge}_{tag}_{options.meson}_{reg}_jet_bin_ALL_Dmeson_track_jet_{options.radius}_{var}_unfold_postFit.root")
                    out_file = ROOT.TFile(out_name, "RECREATE")
                    histograms = defaultdict(list)
                    print(f"Processing {out_name}")
                    for f in files[charge][tag]:
                        print(f"- adding file: {f}")
                        jet_bin = f.split("_jet_bin_")[1][0]
                        hist_file = ROOT.TFile(f"{options.input}/{f}")
                        for key in hist_file.GetListOfKeys():
                            name = key.GetName()
                            if "postFit" in name:
                                h = key.ReadObj()
                                if type(h) == ROOT.TH1D:
                                    if h:
                                        h_clone = deepcopy(h.Clone(f"{charge}_{tag}_{name}_{jet_bin}"))
                                        histograms[name].append(h_clone)
                                    else:
                                        print(f"Empty histogram {name} in {f}")
                        hist_file.Close()
                    out_file.cd()
                    for key, val in histograms.items():
                        if len(val) > 1:
                            h_out = val[0].Clone(key)
                            for h in val[1:]:
                                h_out.Add(h)
                            h_out.Write()
                        else:
                            sample = ""
                            for s in SAMPLES:
                                if s in key:
                                    sample = s
                                    break
                            if not sample:
                                print(f"Sample not found in {key}")
                            h_nominal_list = histograms[f"{sample}_postFit"]
                            h_out = h_nominal_list[0].Clone(f"{key}_tmp0")
                            for i, h in enumerate(h_nominal_list[1:]):
                                h_tmp = h.Clone(f"{key}_tmp{i+1}")
                                h_out.Add(h_tmp)
                            for i in range(1, val[0].GetNbinsX() + 1):
                                if val[0].GetBinContent(i) > 1e-3:
                                    h_out.SetBinContent(i, val[0].GetBinContent(i))
                                    h_out.SetBinError(i, val[0].GetBinError(i))
                            h_out.Write(key)
                    out_file.Close()


if __name__ == "__main__":
    import optparse
    parser = optparse.OptionParser()

    # ----------------------------------------------------
    # arguments
    # ----------------------------------------------------
    parser.add_option('-i', '--input',
                      action="store", dest="input",
                      help="input folder with post-fit ROOT files")
    parser.add_option('-r', '--radius',
                      action="store", dest="radius",
                      help="jet radius")
    parser.add_option('-m', '--meson',
                      action="store", dest="meson",
                      help="jet radius", default="Dplus")

    options, args = parser.parse_args()
    main(options)
