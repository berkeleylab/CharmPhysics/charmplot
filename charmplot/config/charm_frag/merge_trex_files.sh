#!/bin/bash

# Create the output directory if it does not exist
output_dir="/global/cfs/cdirs/atlas/wcharm/trex_frag/charm_frag_dstar_R10_v5/charm_frag_diff/fit"
mkdir -p "$output_dir"

# Define the directories containing the input ROOT files
# dir1="charm_frag_diff_Dmeson_m_fit/fit"
dir1="charm_frag_diff_Dmeson_mdiff_fit/fit"
dir2="charm_frag_diff_Dmeson_zt/fit"
dir3="charm_frag_diff_Dmeson_zl/fit"
dir4="charm_frag_diff_Dmeson_pt/fit"
dir5="charm_frag_diff_Dmeson_pt_rel/fit"
dir6="charm_frag_diff_Dmeson_jet_pt/fit"
dir7="charm_frag_diff_Dmeson_zt_RS/fit"
# dir8="charm_frag_diff_Dmeson_zt_LS/fit"

# Array of file names to be merged
files=(
  "Top_Matched.root"
  "Top_Charm.root"
  "Top_Rest.root"
  "MGPy8EG_FxFx_Wjets_Charm.root"
  "MGPy8EG_FxFx_WplusD_Matched.root"
  "MGPy8EG_FxFx_WplusD_MatchedNoFid.root"
  "Data.root"
  "Multijet_MatrixMethod.root"
  "DibosonZjets_FxFx.root"
  "Sherpa2211_Wjets_Rest.root"
  "MGPy8EG_FxFx_Wjets_MisMatched.root"
  "Offset.root"
  "MockMC_minus_MC.root"
)
  # "MGPy8EG_FxFx_Wjets_Rest.root"

# Merge each file from all four directories
for file in "${files[@]}"; do
  if [[ -f "$dir1/$file" && -f "$dir2/$file" && -f "$dir3/$file" && -f "$dir4/$file" && -f "$dir5/$file" && -f "$dir6/$file" && -f "$dir7/$file" ]]; then
    echo "Merging $file from all directories"
    hadd "$output_dir/$file" "$dir1/$file" "$dir2/$file" "$dir3/$file" "$dir4/$file" "$dir5/$file" "$dir6/$file" "$dir7/$file"
  elif [[ -f "$dir1/$file" ]]; then
    echo "Copying $file from $dir1"
    cp "$dir1/$file" "$output_dir/$file"
  else
    echo "Skipping $file (file not found in any directory)"
  fi
done
