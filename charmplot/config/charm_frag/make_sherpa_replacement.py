import ROOT

f = ROOT.TFile("/global/cfs/cdirs/atlas/shapiro/CharmPlots/templates_spring2024/WcMatchTemplates_ReweightedSherpa_19June.root", "READ")

out = ROOT.TFile("WcMatchTemplates_ReweightedSherpa.root", "RECREATE")

f.cd()
hOS = f.Get("Wcmatch_OS_Bins_Weighted_Sherpa")
hSS = f.Get("Wcmatch_SS_Bins_Weighted_Sherpa")

for i in range(4):
    h_tmp_OS = hOS.Clone(f"SR_0tag_Dplus_OS_PR_jet_bin_{i}__Dmeson_track_jet_10_zt_unfold")
    h_tmp_OS.Reset("ICESM")
    h_tmp_OS.SetNameTitle(f"SR_0tag_Dplus_OS_PR_jet_bin_{i}__Dmeson_track_jet_10_zt_unfold", f"SR_0tag_Dplus_OS_PR_jet_bin_{i}__Dmeson_track_jet_10_zt_unfold")

    h_tmp_SS = hSS.Clone(f"SR_0tag_Dplus_SS_PR_jet_bin_{i}__Dmeson_track_jet_10_zt_unfold")
    h_tmp_SS.Reset("ICESM")
    h_tmp_SS.SetNameTitle(f"SR_0tag_Dplus_SS_PR_jet_bin_{i}__Dmeson_track_jet_10_zt_unfold", f"SR_0tag_Dplus_SS_PR_jet_bin_{i}__Dmeson_track_jet_10_zt_unfold")

    for ib in range(1 + 25 * i, 26 + 25 * i):
        h_tmp_OS.SetBinContent(ib, hOS.GetBinContent(ib))
        h_tmp_OS.SetBinError(ib, hOS.GetBinError(ib))
        h_tmp_SS.SetBinContent(ib, hSS.GetBinContent(ib))
        h_tmp_SS.SetBinError(ib, hSS.GetBinError(ib))

    out.cd()
    h_tmp_OS.Write()
    h_tmp_SS.Write()

out.Close()
f.Close()
