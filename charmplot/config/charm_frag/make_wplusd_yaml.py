from datetime import datetime
import copy
import os
import yaml

data = {
    "data": "Data",
    "samplesConf": "charm_frag",
    "variablesConf": "charmed_wjets",
    "systematics": [
        "experimental",
        "matrix_method",
        "theory_prod_frac",
        "dplus_resolution",
        "mg_fxfx_theory_qcd_fit",
        "sherpa2211_theory_qcd_fit",
        "mg_fxfx_theory_mismatch_alt_sample",
        "mg_fxfx_theory_match_alt_sample",
        "mg_fxfx_theory_nofid_alt_sample",
        "top_theory_qcd",
        "ttbar_theory_alt_samples",
        "sherpa2211_theory_wjets_alt_sample",  # Dstar only
        # "mg_fxfx_theory_wjets_alt_sample",  # Dstar only
    ],
    "channels": {},
}

WJETS_CHARM_SHERPA_TEMPLATE = False
SPLIT_BY_CHARGE = False
CHANNEL = "Dstar"
MOCK_MC = True

WJETS_REST = "MGPy8EG_FxFx_Wjets_Rest"
WJETS_MISMATCH = "MGPy8EG_FxFx_Wjets_MisMatched"
if CHANNEL == "Dstar":
    WJETS_REST = "Sherpa2211_Wjets_Rest"
    # WJETS_MISMATCH = "Sherpa2211_Wjets_MisMatched"
    data["samplesConf"] = "charm_frag_dstar"

for sign in ["OS", "SS"]:
    for tag in ["0tag", "1tag", "2tag"]:
        for reg in ["PR", "LS", "RS", ""]:
            for jet in ["jet_bin_0", "jet_bin_1", "jet_bin_2", "jet_bin_3", ""]:
                # for jet in [""]:
                reg_jet = f"{'' if reg == '' else f'_{reg}'}{'' if jet == '' else f'_{jet}'}"
                name = f"{sign}_{tag}_{CHANNEL}{reg_jet}"
                channel = {
                    'force_positive': False,
                    'lumi': '2015+2016+2017+2018',
                    'save_to_file': True,
                    'label': [
                        f'W(#rightarrowl#nu) + D(#rightarrowK#pi#pi), {sign}',
                        f"W^{{#pm}}, {tag}",
                    ],
                    'replacement_samples': {
                        # WJETS_REST: f"OSSS{name[2:]}_Rest",
                        WJETS_REST: f"{name}_Rest_Loose",
                        WJETS_MISMATCH: f"{name}_MisMatched_Loose",
                    },
                }
                if reg:
                    channel['label'][1] += f", {reg}"
                if jet:
                    channel['label'][1] += f", {jet}"

                # replace W+c template
                if WJETS_CHARM_SHERPA_TEMPLATE and tag == "0tag":
                    channel['replacement_samples']["MGPy8EG_FxFx_Wjets_Charm"] = f"{name}_Sherpa_Template"

                # regions
                if SPLIT_BY_CHARGE:
                    regions = [f"{lep}_{charge}_SR_{tag}_{CHANNEL}_{sign}{reg_jet}" for lep in ["mu", "el"] for charge in ["plus", "minus"]]
                else:
                    regions = [f"{lep}_SR_{tag}_{CHANNEL}_{sign}{reg_jet}" for lep in ["mu", "el"]]
                if reg == '':
                    regions = [x.replace(f"_{sign}", f"_{sign}_{y}") for x in regions for y in ["PR", "LS", "RS"]]
                if jet == '':
                    regions = [f"{x}_{y}" for x in regions for y in ["jet_bin_0", "jet_bin_1", "jet_bin_2", "jet_bin_3"]]
                channel['regions'] = regions

                # samples
                if tag == "0tag":
                    samples = [
                        f'MGPy8EG_FxFx_WplusD_Matched | {name}_Matched',
                        f'MGPy8EG_FxFx_WplusD_MatchedNoFid | {name}_MatchedNoFid',
                        f'MGPy8EG_FxFx_Wjets_Charm | {name}_MatchedCharm',
                        f'{WJETS_MISMATCH} | {name}_MisMatched',
                        f'{WJETS_REST} | {name}_Rest',
                        f'Top_Matched | {name}_Matched',
                        f'Top_Charm | {name}_Charm',
                        f'Top_Rest | {name}_Rest',
                        'DibosonZjets_FxFx',
                        f'Multijet_MatrixMethod | {name}_MatrixMethod',
                    ]
                else:
                    samples = [
                        f'Top_Matched | {name}_Matched',
                        f'Top_Charm | {name}_Charm',
                        f'Top_Rest | {name}_Rest',
                        f'MGPy8EG_FxFx_WplusD_Matched | {name}_Matched',
                        f'MGPy8EG_FxFx_WplusD_MatchedNoFid | {name}_MatchedNoFid',
                        f'MGPy8EG_FxFx_Wjets_Charm | {name}_MatchedCharm',
                        f'{WJETS_MISMATCH} | {name}_MisMatched',
                        f'{WJETS_REST} | {name}_Rest',
                        'DibosonZjets_FxFx',
                        f'Multijet_MatrixMethod | {name}_MatrixMethod',
                    ]
                if MOCK_MC and reg == "" and jet != "":
                    samples += [
                        f"MockMC_minus_MC | {name}_MockMC",
                        f"Offset | {name}_Offset",
                    ]
                channel['samples'] = samples

                # add to data
                data["channels"][name] = channel

                # auxiliary channels
                channel_matched = {
                    "make_plots": False,
                    "save_to_file": False,
                    "regions": [f"{x}_Matched" for x in regions],
                }

                channel_matched_nofid = {
                    "make_plots": False,
                    "save_to_file": False,
                    "regions": [f"{x}_MatchedNoFid" for x in regions],
                }

                channel_matched_charm = {
                    "make_plots": False,
                    "save_to_file": False,
                    # "regions": [f"{x}_{y}" for x in regions for y in ["CharmMisMatched", "MatchedNoFid"]],
                    # "regions": [f"{x}_{y}" for x in regions for y in ["CharmMisMatched"]],
                    "regions": [f"{x}_{y}" for x in regions for y in ["411MisMatched", "413MisMatched", "421MisMatched", "431MisMatched", "BaryonMisMatched"]],
                }

                channel_mis_matched = {
                    "make_plots": False,
                    "save_to_file": False,
                    "regions": [f"{x}_MisMatched" for x in regions],
                }

                channel_mis_matched_loose = {
                    "make_plots": False,
                    "save_to_file": False,
                    "regions": [f"{x}_MisMatched" for x in regions] + [f"{x.replace('_SR_', '_Anti_SR_')}_MisMatched" for x in regions],
                    "samples": [WJETS_MISMATCH]
                }

                channel_rest = {
                    "make_plots": False,
                    "save_to_file": False,
                    "regions": [f"{x}_{y}" for x in regions for y in ["HardMisMatched", "Other"]],
                }

                channel_rest_loose = {
                    "make_plots": False,
                    "save_to_file": False,
                    "regions": [f"{x}_{y}" for x in regions for y in ["HardMisMatched", "Other"]] + [f"{x.replace('_SR_', '_Anti_SR_')}_{y}" for x in regions for y in ["HardMisMatched", "Other"]],
                    "samples": [WJETS_REST]
                }

                channel_all_charm = {
                    "make_plots": False,
                    "save_to_file": False,
                    "regions": [f"{x}_{y}" for x in regions for y in ["MisMatched", "411MisMatched", "413MisMatched", "421MisMatched", "431MisMatched", "BaryonMisMatched"]],
                }

                channel_matrix_method = {
                    "make_plots": False,
                    "save_to_file": False,
                    "regions": [f"AntiTight_{x}" for x in regions] + [f"-Tight_{x}" for x in regions],
                }

                data["channels"][f"{name}_Matched"] = channel_matched
                data["channels"][f"{name}_MatchedNoFid"] = channel_matched_nofid
                data["channels"][f"{name}_MatchedCharm"] = channel_matched_charm
                data["channels"][f"{name}_MisMatched"] = channel_mis_matched
                data["channels"][f"{name}_MisMatched_Loose"] = channel_mis_matched_loose
                data["channels"][f"{name}_Rest"] = channel_rest
                data["channels"][f"{name}_Rest_Loose"] = channel_rest_loose
                data["channels"][f"{name}_Charm"] = channel_all_charm
                data["channels"][f"{name}_MatrixMethod"] = channel_matrix_method

                if WJETS_CHARM_SHERPA_TEMPLATE and tag == "0tag":
                    channel_matched_charm_sherpa_template = {
                        "make_plots": False,
                        "save_to_file": False,
                        "samples": ["MGPy8EG_FxFx_Wjets_Charm_Sherpa_Template"],
                    }
                    if SPLIT_BY_CHARGE:
                        channel_matched_charm_sherpa_template["regions"] = [f"{x.replace('el_minus_', '')}" for x in regions if "el_minus" in x]
                    else:
                        channel_matched_charm_sherpa_template["regions"] = [f"{x.replace('el_', '')}" for x in regions if "el_" in x]
                    data["channels"][f"{name}_Sherpa_Template"] = channel_matched_charm_sherpa_template

                if reg == "" and jet != "":
                    channel_mock_mc = {
                        "make_plots": False,
                        "save_to_file": False,
                    }
                    if SPLIT_BY_CHARGE:
                        channel_mock_mc["regions"] = [f"{x.replace('el_minus_', '').replace('PR_', '')}_MockMC" for x in regions if "el_minus" in x and "PR" in x]
                    else:
                        channel_mock_mc["regions"] = [f"{x.replace('el_', '').replace('PR_', '')}_MockMC" for x in regions if "el_" in x and "PR" in x]

                    channel_offset = {
                        "make_plots": False,
                        "save_to_file": False,
                    }
                    if SPLIT_BY_CHARGE:
                        channel_offset["regions"] = [f"{x.replace('el_minus_', '').replace('PR_', '')}_Offset" for x in regions if "el_minus" in x and "PR" in x]
                    else:
                        channel_offset["regions"] = [f"{x.replace('el_', '').replace('PR_', '')}_Offset" for x in regions if "el_" in x and "PR" in x]

                    data["channels"][f"{name}_MockMC"] = channel_mock_mc
                    data["channels"][f"{name}_Offset"] = channel_offset

                # OS-SS
                if sign == "OS":
                    channel_os_ss = copy.deepcopy(channel)
                    channel_os_ss["label"][0] = channel_os_ss["label"][0].replace("OS", "OS-SS")
                    channel_os_ss["samples"] = [sample.replace(name, name.replace("OS", "OS-SS"))
                                                for sample in channel_os_ss["samples"] if ("MockMC" not in sample and "Offset" not in sample)]
                    data["channels"][name.replace("OS", "OS-SS")] = channel_os_ss
                    channel_os_ss["regions"] += [f"-{region.replace('OS', 'SS')}" for region in channel_os_ss["regions"]]

                    channel_matched_os_ss = copy.deepcopy(channel_matched)
                    channel_matched_os_ss["regions"] += [f"-{region.replace('OS', 'SS')}" for region in channel_matched_os_ss["regions"]]
                    data["channels"][f"{name}_Matched".replace("OS", "OS-SS")] = channel_matched_os_ss

                    channel_matched_nofid_os_ss = copy.deepcopy(channel_matched_nofid)
                    channel_matched_nofid_os_ss["regions"] += [f"-{region.replace('OS', 'SS')}" for region in channel_matched_nofid_os_ss["regions"]]
                    data["channels"][f"{name}_MatchedNoFid".replace("OS", "OS-SS")] = channel_matched_nofid_os_ss

                    channel_matched_charm_os_ss = copy.deepcopy(channel_matched_charm)
                    channel_matched_charm_os_ss["regions"] += [f"-{region.replace('OS', 'SS')}" for region in channel_matched_charm_os_ss["regions"]]
                    data["channels"][f"{name}_MatchedCharm".replace("OS", "OS-SS")] = channel_matched_charm_os_ss

                    channel_mis_matched_os_ss = copy.deepcopy(channel_mis_matched)
                    channel_mis_matched_os_ss["regions"] += [f"-{region.replace('OS', 'SS')}" for region in channel_mis_matched_os_ss["regions"]]
                    data["channels"][f"{name}_MisMatched".replace("OS", "OS-SS")] = channel_mis_matched_os_ss

                    channel_mis_matched_loose_os_ss = copy.deepcopy(channel_mis_matched_loose)
                    channel_mis_matched_loose_os_ss["regions"] += [f"-{region.replace('OS', 'SS')}" for region in channel_mis_matched_loose_os_ss["regions"]]
                    data["channels"][f"{name}_MisMatched_Loose".replace("OS", "OS-SS")] = channel_mis_matched_loose_os_ss

                    channel_rest_os_ss = copy.deepcopy(channel_rest)
                    channel_rest_os_ss["regions"] += [f"-{region.replace('OS', 'SS')}" for region in channel_rest_os_ss["regions"]]
                    data["channels"][f"{name}_Rest".replace("OS", "OS-SS")] = channel_rest_os_ss

                    channel_rest_loose_os_ss = copy.deepcopy(channel_rest_loose)
                    channel_rest_loose_os_ss["regions"] += [f"-{region.replace('OS', 'SS')}" for region in channel_rest_loose_os_ss["regions"]]
                    data["channels"][f"{name}_Rest_Loose".replace("OS", "OS-SS")] = channel_rest_loose_os_ss

                    channel_all_charm_os_ss = copy.deepcopy(channel_all_charm)
                    channel_all_charm_os_ss["regions"] += [f"-{region.replace('OS', 'SS')}" for region in channel_all_charm_os_ss["regions"]]
                    data["channels"][f"{name}_Charm".replace("OS", "OS-SS")] = channel_all_charm_os_ss

                    channel_matrix_method_os_ss = copy.deepcopy(channel_matrix_method)
                    for region in copy.copy(channel_matrix_method_os_ss["regions"]):
                        if region.startswith("-"):
                            region = region[1:]
                        else:
                            region = f"-{region}"
                        channel_matrix_method_os_ss["regions"] += [f"{region.replace('OS', 'SS')}"]
                    data["channels"][f"{name}_MatrixMethod".replace("OS", "OS-SS")] = channel_matrix_method_os_ss

                    # replacement samples
                    channel_rest_osss = copy.deepcopy(channel_rest_os_ss)
                    channel_rest_osss["regions"] = [region[1:] if region.startswith("-") else region for region in channel_rest_osss["regions"]]
                    data["channels"][f"{name}_Rest".replace("OS", "OSSS")] = channel_rest_osss

                    if WJETS_CHARM_SHERPA_TEMPLATE and tag == "0tag":
                        channel_matched_charm_sherpa_template_os_ss = copy.deepcopy(channel_matched_charm_sherpa_template)
                        channel_matched_charm_sherpa_template_os_ss["regions"] += [f"-{region.replace('OS', 'SS')}" for region in channel_matched_charm_sherpa_template_os_ss["regions"]]
                        data["channels"][f"{name}_Sherpa_Template".replace("OS", "OS-SS")] = channel_matched_charm_sherpa_template_os_ss
                        channel_os_ss["replacement_samples"]["MGPy8EG_FxFx_Wjets_Charm"] = f"OS-SS{name[2:]}_Sherpa_Template"

                    channel_os_ss["replacement_samples"][WJETS_MISMATCH] = f"OS-SS{name[2:]}_MisMatched_Loose"
                    channel_os_ss["replacement_samples"][WJETS_REST] = f"OS-SS{name[2:]}_Rest_Loose"

# Metadata information
script_name = os.path.basename(__file__)
timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
header = f"# Generated by {script_name} on {timestamp}\n"

with open('charm_frag_diff.yaml', 'w') as file:
    file.write(header)
    yaml.dump(data, file, sort_keys=False)
