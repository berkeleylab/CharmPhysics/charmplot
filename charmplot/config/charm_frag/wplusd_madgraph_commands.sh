python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_data_mc.py -a charm_frag/wplusd_madgraph_fit -v Dmeson_m_fit,Dmeson_pt,Dmeson_pt_wide,Dmeson_track_jet_10_zt,Dmeson_track_jet_10_zt_wide,Dmeson_track_jet_10_pt,Dmeson_track_jet_10_pt_wide -c OS-SS_0tag_Dplus,OS-SS_1tag_Dplus,OS-SS_2tag_Dplus,OS_0tag_Dplus,OS_1tag_Dplus,OS_2tag_Dplus,SS_0tag_Dplus,SS_1tag_Dplus,SS_2tag_Dplus,OS-SS_0tag_Dplus_PR,OS-SS_1tag_Dplus_PR,OS-SS_2tag_Dplus_PR,OS_0tag_Dplus_PR,OS_1tag_Dplus_PR,OS_2tag_Dplus_PR,SS_0tag_Dplus_PR,SS_1tag_Dplus_PR,SS_2tag_Dplus_PR,OS-SS_0tag_Dplus_LS,OS-SS_1tag_Dplus_LS,OS-SS_2tag_Dplus_LS,OS_0tag_Dplus_LS,OS_1tag_Dplus_LS,OS_2tag_Dplus_LS,SS_0tag_Dplus_LS,SS_1tag_Dplus_LS,SS_2tag_Dplus_LS,OS-SS_0tag_Dplus_RS,OS-SS_1tag_Dplus_RS,OS-SS_2tag_Dplus_RS,OS_0tag_Dplus_RS,OS_1tag_Dplus_RS,OS_2tag_Dplus_RS,SS_0tag_Dplus_RS,SS_1tag_Dplus_RS,SS_2tag_Dplus_RS --trex fit

python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_data_mc.py -a charm_frag/wplusd_madgraph_fit -v Dmeson_m_fit,Dmeson_track_jet_10_zt,Dmeson_track_jet_10_zt_unfold -c OS-SS_0tag_Dplus,OS_0tag_Dplus,SS_0tag_Dplus,OS-SS_0tag_Dplus_PR,OS_0tag_Dplus_PR,SS_0tag_Dplus_PR,OS-SS_0tag_Dplus_LS,OS_0tag_Dplus_LS,SS_0tag_Dplus_LS,OS-SS_0tag_Dplus_RS,OS_0tag_Dplus_RS,SS_0tag_Dplus_RS --trex fit --suffix fit

python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_data_mc.py -a charm_frag/wplusd_madgraph_fit -v Dmeson_m,Dmeson_m_fit,Dmeson_pt,Dmeson_track_jet_10_zt,Dmeson_track_jet_10_pt,Dmeson_track_jet_10_zt_unfold -c OS-SS_0tag_Dplus,OS-SS_1tag_Dplus,OS-SS_2tag_Dplus,OS-SS_0tag_Dplus_PR,OS-SS_1tag_Dplus_PR,OS-SS_2tag_Dplus_PR

python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_mc_mc.py -a charm_frag/wplusd_madgraph_fit_differential -v Dmeson_m_fit -c OS-SS_0tag_Dplus -s MGPy8EG_FxFx_WplusD_Matched,MGPy8EG_FxFx_Wjets_Matched

python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_data_mc.py -a charm_frag/wplusd_madgraph_fit -v Dmeson_m,Dmeson_m_fit,Dmeson_pt,Dmeson_track_jet_10_zt,Dmeson_track_jet_10_pt,Dmeson_track_jet_10_zt_unfold -c OS-SS_0tag_Dplus,OS-SS_0tag_Dplus_PR,OS_0tag_Dplus,OS_0tag_Dplus_PR,SS_0tag_Dplus,SS_0tag_Dplus_PR

python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_data_mc.py -a charm_frag/wplusd_madgraph_fit -v Dmeson_m_fit -c SS_0tag_Dplus_PR,SS_1tag_Dplus_PR,SS_2tag_Dplus_PR,SS_0tag_Dplus_LS,SS_1tag_Dplus_LS,SS_2tag_Dplus_LS,SS_0tag_Dplus_RS,SS_1tag_Dplus_RS,SS_2tag_Dplus_RS --suffix mock_mc

python /global/homes/m/mmuskinj/work/Charm/charmplot/macros/make_offset_and_symm_histograms.py -i charm_frag_mock_mc/wplusd_madgraph_fit/histograms.root -d Dplus

python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_mc_mc.py -a charm_frag/wplusd_madgraph_fit -v Dmeson_transfer_matrix_track_jet_10_zt,Dmeson_track_jet_10_zt -c OS-SS_0tag_Dplus_PR -s MGPy8EG_FxFx_WplusD_Matched --suffix matrix

pthon /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_mc_mc.py -a truth/wplusd_truth_analysis -s MGPy8EG_FxFx_WplusD -c OS-SS_Dplus_Kpipi -v D_pt,D_jet_10_zt,D_jet_10_pt --no-sys

python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_post_fit.py -a charm_frag/wplusd_madgraph_fit --trex-input /pscratch/sd/m/mmuskinj/run/trex/charm_frag_5/wplusd_madgraph -v Dmeson_m_fit
python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_post_fit.py -a charm_frag/wplusd_madgraph_fit --trex-input /pscratch/sd/m/mmuskinj/run/trex/charm_frag_5/wplusd_madgraph -v Dmeson_pt
python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_post_fit.py -a charm_frag/wplusd_madgraph_fit --trex-input /pscratch/sd/m/mmuskinj/run/trex/charm_frag_5/wplusd_madgraph -v Dmeson_pt_wide
python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_post_fit.py -a charm_frag/wplusd_madgraph_fit --trex-input /pscratch/sd/m/mmuskinj/run/trex/charm_frag_5/wplusd_madgraph -v Dmeson_track_jet_10_zt
python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_post_fit.py -a charm_frag/wplusd_madgraph_fit --trex-input /pscratch/sd/m/mmuskinj/run/trex/charm_frag_5/wplusd_madgraph -v Dmeson_track_jet_10_zt_wide
python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_post_fit.py -a charm_frag/wplusd_madgraph_fit --trex-input /pscratch/sd/m/mmuskinj/run/trex/charm_frag_5/wplusd_madgraph -v Dmeson_track_jet_10_pt
python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_post_fit.py -a charm_frag/wplusd_madgraph_fit --trex-input /pscratch/sd/m/mmuskinj/run/trex/charm_frag_5/wplusd_madgraph -v Dmeson_track_jet_10_pt_wide

python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_post_fit.py -a charm_frag/wplusd_madgraph_fit --trex-input /pscratch/sd/m/mmuskinj/run/trex/charm_frag_5/wplusd_madgraph -v Dmeson_pt -b -k "1tag,2tag" --suffix sub
python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_post_fit.py -a charm_frag/wplusd_madgraph_fit --trex-input /pscratch/sd/m/mmuskinj/run/trex/charm_frag_5/wplusd_madgraph -v Dmeson_pt -b -k "0tag" -s Top_Matched --suffix sub
python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_post_fit.py -a charm_frag/wplusd_madgraph_fit --trex-input /pscratch/sd/m/mmuskinj/run/trex/charm_frag_5/wplusd_madgraph -v Dmeson_track_jet_10_zt -b -k "1tag,2tag" --suffix sub
python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_post_fit.py -a charm_frag/wplusd_madgraph_fit --trex-input /pscratch/sd/m/mmuskinj/run/trex/charm_frag_5/wplusd_madgraph -v Dmeson_track_jet_10_zt -b -k "0tag" -s Top_Matched --suffix sub
python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_post_fit.py -a charm_frag/wplusd_madgraph_fit --trex-input /pscratch/sd/m/mmuskinj/run/trex/charm_frag_5/wplusd_madgraph -v Dmeson_track_jet_10_pt -b -k "1tag,2tag" --suffix sub
python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_post_fit.py -a charm_frag/wplusd_madgraph_fit --trex-input /pscratch/sd/m/mmuskinj/run/trex/charm_frag_5/wplusd_madgraph -v Dmeson_track_jet_10_pt -b -k "0tag" -s Top_Matched --suffix sub




python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_post_fit.py -a charm_frag/wplusd_madgraph_fit --trex-input /pscratch/sd/m/mmuskinj/run/trex/charm_frag_5/wplusd_madgraph -v Dmeson_m_fit
python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_post_fit.py -a charm_frag/wplusd_madgraph_fit --trex-input /pscratch/sd/m/mmuskinj/run/trex/charm_frag_5/wplusd_madgraph -v Dmeson_track_jet_10_zt
python /global/homes/m/mmuskinj/work/Charm/charmplot/charmplot/scripts/plot_post_fit.py -a charm_frag/wplusd_madgraph_fit --trex-input /pscratch/sd/m/mmuskinj/run/trex/charm_frag_5/wplusd_madgraph -v Dmeson_track_jet_10_zt_unfold
