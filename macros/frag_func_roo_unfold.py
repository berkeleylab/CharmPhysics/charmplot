#!/usr/bin/env python
import os
import ROOT

# ATLAS Style
dirname = os.path.join(os.path.dirname(__file__), "../atlasrootstyle")
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.LoadMacro(os.path.join(dirname, "AtlasStyle.C"))
ROOT.gROOT.LoadMacro(os.path.join(dirname, "AtlasLabels.C"))
ROOT.gROOT.LoadMacro(os.path.join(dirname, "AtlasUtils.C"))
ROOT.SetAtlasStyle()

MATRIX_PATH = "/global/cfs/cdirs/atlas/wcharm/charmpp_frag/unfolding/v1/charm_frag_matrix/wplusd_madgraph_fit/histograms.root"
TRUTH_PATH = "/global/cfs/cdirs/atlas/wcharm/charmpp_frag/unfolding/v1/truth/wplusd_truth_analysis/histograms.root"
DATA_PATH = "/global/cfs/cdirs/atlas/wcharm/charmpp_frag/unfolding/v1/post_fit/charm_frag_sub/wplusd_madgraph_fit/outpoot.root"

# Full Run 2 lumi
LUMI = 58450.1 + 44630.6 + 3244.54 + 33402.2

# make output directory
if not os.path.exists("unfolding"):
    os.makedirs("unfolding")


def createCanvasPads(name):
    c = ROOT.TCanvas(name, name, 1200, 1200)
    # Upper histogram plot is pad1
    pad1 = ROOT.TPad(f"pad1_{name}", f"pad1_{name}", 0, 0.30, 1, 1.0)
    pad1.SetTopMargin(0.1)
    pad1.SetBottomMargin(0.05 / 0.70)
    pad1.SetFillStyle(4000)
    pad1.Draw()
    # Lower ratio plot is pad2
    c.cd()
    pad2 = ROOT.TPad(f"pad2_{name}", f"pad2_{name}", 0, 0.00, 1, 0.40)
    pad2.SetTopMargin(0.05 / 0.40)
    pad2.SetBottomMargin(0.10 / 0.40)
    pad2.SetFillStyle(4000)
    pad2.Draw()

    return c, pad1, pad2


def configure_axis(h_up, h_dn):
    GLOBAL_SF = 1.1
    h_up.GetYaxis().SetTitleSize(h_up.GetYaxis().GetTitleSize() * GLOBAL_SF)
    h_up.GetYaxis().SetTitleOffset(h_up.GetYaxis().GetTitleOffset() * (1 / (GLOBAL_SF * 1.1)))
    h_up.GetYaxis().SetLabelSize(h_up.GetYaxis().GetLabelSize() * GLOBAL_SF)
    h_up.GetXaxis().SetLabelSize(0)

    SF = 0.70 / 0.40
    h_dn.GetYaxis().SetTitleSize(h_dn.GetYaxis().GetTitleSize() * SF * GLOBAL_SF)
    h_dn.GetYaxis().SetTitleOffset(h_dn.GetYaxis().GetTitleOffset() * (1 / (GLOBAL_SF * 1.06 * SF)))
    h_dn.GetXaxis().SetTitleSize(h_dn.GetXaxis().GetTitleSize() * SF * GLOBAL_SF)
    h_dn.GetXaxis().SetTitleOffset(h_dn.GetXaxis().GetTitleOffset() * (1 / (GLOBAL_SF * 0.6 * SF)))
    h_dn.GetXaxis().SetLabelOffset(h_dn.GetXaxis().GetLabelOffset() * 4.0)
    h_dn.GetYaxis().SetLabelSize(h_dn.GetYaxis().GetLabelSize() * SF * GLOBAL_SF)
    h_dn.GetXaxis().SetLabelSize(h_dn.GetXaxis().GetLabelSize() * SF * GLOBAL_SF)

    # tick marks
    h_up.GetYaxis().SetNdivisions(506)
    h_dn.GetYaxis().SetNdivisions(306)


def main():
    # response matrix
    f_matrix = ROOT.TFile(MATRIX_PATH, "READ")
    h_matrix = f_matrix.Get("MGPy8EG_FxFx_WplusD_Matched_OS-SS_0tag_Dplus_PR_Dmeson_transfer_matrix_track_jet_10_zt")
    h_reco = f_matrix.Get("MGPy8EG_FxFx_WplusD_Matched_OS-SS_0tag_Dplus_PR_Dmeson_track_jet_10_zt")

    # truth distribution
    f_truth = ROOT.TFile(TRUTH_PATH, "READ")
    h_truth = f_truth.Get("MGPy8EG_FxFx_WplusD_OS-SS_Dplus_Kpipi_D_jet_10_zt")

    # data distribution
    f_data = ROOT.TFile(DATA_PATH, "READ")
    h_data = f_data.Get("OS_0tag_Dplus_PR_SS_0tag_Dplus_PR_Dmeson_track_jet_10_zt_data")

    # remake histograms
    h_matrix_rebin = ROOT.TH2D("h_matrix_rebin", "h_matrix_rebin", 25, 0.0, 1, 25, 0.0, 1)
    h_truth_rebin = ROOT.TH1D("h_truth_rebin", "h_truth_rebin", 25, 0.0, 1)
    h_reco_rebin = ROOT.TH1D("h_reco_rebin", "h_reco_rebin", 25, 0.0, 1)
    h_data_rebin = ROOT.TH1D("h_data_rebin", "h_data_rebin", 25, 0.0, 1)

    # first bin of graph
    N = 0
    for i in range(0, 25):
        if h_data.GetX()[i] > 0.0:
            N = i
            break

    for i in range(1, 26):
        h_truth_rebin.SetBinContent(i, h_truth.GetBinContent(h_truth.FindBin(h_truth_rebin.GetBinCenter(i))))
        h_truth_rebin.SetBinError(i, h_truth.GetBinError(h_truth.FindBin(h_truth_rebin.GetBinCenter(i))))
        h_reco_rebin.SetBinContent(i, h_reco.GetBinContent(h_reco.FindBin(h_reco_rebin.GetBinCenter(i))))
        h_reco_rebin.SetBinError(i, h_reco.GetBinError(h_reco.FindBin(h_reco_rebin.GetBinCenter(i))))
        h_data_rebin.SetBinContent(i, h_data.GetY()[N + i - 1])
        h_data_rebin.SetBinError(i, h_data.GetEY()[N + i - 1])
        for j in range(1, 26):
            x = h_matrix_rebin.GetXaxis().GetBinCenter(i)
            y = h_matrix_rebin.GetYaxis().GetBinCenter(j)
            h_matrix_rebin.SetBinContent(i, j, h_matrix.GetBinContent(h_matrix.FindBin(x, y)))

    # normalize turth to reco
    h_truth_rebin.Scale(h_reco_rebin.Integral() / h_truth_rebin.Integral())

    # # normalize truth to the number of expected events
    # # 0.094 is Br(D+ -> K pi pi) due to the forced decay sample
    # h_truth_rebin.Scale(LUMI * 0.094)

    # unfold
    response = ROOT.RooUnfoldResponse(h_reco_rebin, h_truth_rebin, h_matrix_rebin)
    unfold = ROOT.RooUnfoldBayes(response, h_reco_rebin, 4)

    # results
    h_unfolded = unfold.Hunfold()

    # colors
    h_truth_rebin.SetLineColor(ROOT.kBlue)
    h_reco_rebin.SetLineColor(ROOT.kRed)

    # ratios
    h_truth_ratio = h_truth_rebin.Clone()
    h_truth_ratio.Divide(h_truth_rebin)
    h_unfolded_ratio = h_unfolded.Clone()
    h_unfolded_ratio.Divide(h_truth_rebin)

    # legend
    N = 3
    leg = ROOT.TLegend(0.2, 0.68 - N / 2. * (0.125), 0.4, 0.68)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.SetFillStyle(0)
    leg.SetTextSize(42)
    leg.SetTextFont(43)
    leg.AddEntry(h_truth_rebin, "Truth", "l")
    leg.AddEntry(h_reco_rebin, "Reco", "l")
    leg.AddEntry(h_unfolded, "Unfolded", "l")

    # canvas
    canv, pad1, pad2 = createCanvasPads("unfolded")
    configure_axis(h_truth_rebin, h_truth_ratio)

    # labels
    h_truth_rebin.GetYaxis().SetTitle("dN/dz_{T}")
    h_truth_ratio.GetYaxis().SetTitle("Ratio")
    h_truth_ratio.GetXaxis().SetTitle("track-jet z_{T}")

    pad1.cd()
    h_truth_rebin.Draw("hist")
    h_reco_rebin.Draw("hist same")
    h_unfolded.Draw("same")
    leg.Draw()

    # ATLAS label
    ROOT.ATLASLabel(0.20, 0.82, "Internal", 1, 0.05)
    ROOT.myText(0.20, 0.77, 1, "#sqrt{s} = 13 TeV", 0.05)
    ROOT.myText(0.20, 0.72, 1, "W+D(#rightarrowK#pi#pi)", 0.05)

    pad2.cd()
    h_truth_ratio.Draw("hist")
    h_unfolded_ratio.Draw("same pe")
    h_truth_ratio.GetYaxis().SetRangeUser(0.71, 1.29)

    canv.Print("unfolding/unfolded.pdf")

    # output file
    f_out = ROOT.TFile("unfolding/unfold.root", "RECREATE")
    h_truth_rebin.Write()
    h_reco_rebin.Write()
    h_matrix_rebin.Write()
    h_data_rebin.Write()
    f_out.Close()


if __name__ == "__main__":

    # run
    main()
