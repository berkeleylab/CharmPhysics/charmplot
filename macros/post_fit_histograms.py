#!/usr/bin/env python
import os
import ROOT

# ATLAS Style
dirname = os.path.join(os.path.dirname(__file__), "../atlasrootstyle")
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.LoadMacro(os.path.join(dirname, "AtlasStyle.C"))
ROOT.SetAtlasStyle()


def divide_without_errors(h1, h2, name):
    h = h1.Clone(name)
    for i in range(1, h.GetNbinsX() + 1):
        y = h2.GetBinContent(i)
        if y != 0:
            h.SetBinContent(i, h1.GetBinContent(i) / h2.GetBinContent(i))
            h.SetBinError(i, h1.GetBinError(i) / h2.GetBinContent(i))
        else:
            h.SetBinContent(i, 0)
            h.SetBinError(i, 0)
    return h


def createCanvasPads(name):
    c = ROOT.TCanvas(name, name, 1200, 1200)
    # Upper histogram plot is pad1
    pad1 = ROOT.TPad(f"pad1_{name}", f"pad1_{name}", 0, 0.30, 1, 1.0)
    pad1.SetTopMargin(0.1)
    pad1.SetBottomMargin(0.05 / 0.70)
    pad1.SetFillStyle(4000)
    pad1.Draw()
    # Lower ratio plot is pad2
    c.cd()
    pad2 = ROOT.TPad(f"pad2_{name}", f"pad2_{name}", 0, 0.00, 1, 0.40)
    pad2.SetTopMargin(0.05 / 0.40)
    pad2.SetBottomMargin(0.10 / 0.40)
    pad2.SetFillStyle(4000)
    pad2.Draw()

    return c, pad1, pad2


def configure_axis(h_up, h_dn):
    GLOBAL_SF = 1.0
    h_up.GetYaxis().SetTitleSize(h_up.GetYaxis().GetTitleSize() * GLOBAL_SF)
    h_up.GetYaxis().SetTitleOffset(h_up.GetYaxis().GetTitleOffset() * (1 / (GLOBAL_SF * 1.1)))
    h_up.GetYaxis().SetLabelSize(h_up.GetYaxis().GetLabelSize() * GLOBAL_SF)
    h_up.GetXaxis().SetLabelSize(0)

    SF = 0.70 / 0.40
    h_dn.GetYaxis().SetTitleSize(h_dn.GetYaxis().GetTitleSize() * SF * GLOBAL_SF)
    h_dn.GetYaxis().SetTitleOffset(h_dn.GetYaxis().GetTitleOffset() * (1 / (GLOBAL_SF * 1.06 * SF)))
    h_dn.GetXaxis().SetTitleSize(h_dn.GetXaxis().GetTitleSize() * SF * GLOBAL_SF)
    h_dn.GetXaxis().SetTitleOffset(h_dn.GetXaxis().GetTitleOffset() * (1 / (GLOBAL_SF * 0.6 * SF)))
    h_dn.GetXaxis().SetLabelOffset(h_dn.GetXaxis().GetLabelOffset() * 4.0)
    h_dn.GetYaxis().SetLabelSize(h_dn.GetYaxis().GetLabelSize() * SF * GLOBAL_SF)
    h_dn.GetXaxis().SetLabelSize(h_dn.GetXaxis().GetLabelSize() * SF * GLOBAL_SF)

    # tick marks
    h_up.GetYaxis().SetNdivisions(506)
    h_dn.GetYaxis().SetNdivisions(306)


def atlas_label(labels=[], internal="Internal"):
    l1 = ROOT.TLatex()
    l1.SetTextFont(73)
    l1.SetTextSize(42)
    l1.DrawLatexNDC(0.18, 0.82, "ATLAS")
    l2 = ROOT.TLatex()
    l2.SetTextFont(43)
    l2.SetTextSize(42)
    l2.DrawLatexNDC(0.31, 0.82, internal)
    for i, lab in enumerate(labels):
        l2.DrawLatexNDC(0.18, 0.82 - (i + 1) * 0.06, lab)


def make_legend(N, x1=0.65, x2=0.92, y2=0.848, w=0.06, size=42):
    leg = ROOT.TLegend(x1, y2 - N * w, x2, y2)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.SetFillStyle(0)
    leg.SetTextSize(size)
    leg.SetTextFont(43)
    return leg


def get_bkg_hist(file_os, file_ss, np, direction):
    mc_bkg = file_os.Get(f"h_tot_{np}_{direction}_postFit").Clone(f"mc_tot_{np}_{direction}")
    mc_sig = file_os.Get(f"h_MGPy8EG_FxFx_WplusD_Matched_{np}_{direction}_postFit").Clone(f"mc_sig_{np}_{direction}")
    mc_bkg_ss = file_ss.Get(f"h_tot_{np}_{direction}_postFit").Clone(f"mc_bkg_ss_{np}_{direction}")
    mc_sig_ss = file_ss.Get(f"h_MGPy8EG_FxFx_WplusD_Matched_{np}_{direction}_postFit").Clone(f"mc_sig_ss_{np}_{direction}")
    mc_bkg.Add(mc_bkg_ss, -1)
    mc_sig.Add(mc_sig_ss, -1)
    mc_bkg.Add(mc_sig, -1)
    mc_bkg.SetMarkerSize(0)
    mc_sig.SetMarkerSize(0)
    if direction == "Up":
        mc_bkg.SetLineColor(ROOT.kRed)
        mc_sig.SetLineColor(ROOT.kRed)
    else:
        mc_bkg.SetLineColor(ROOT.kBlue)
        mc_sig.SetLineColor(ROOT.kBlue)

    # set stat uncertainty to zero in variations
    for i in range(1, mc_bkg.GetNbinsX() + 1):
        mc_bkg.SetBinError(i, 1e-6)
        mc_sig.SetBinError(i, 1e-6)

    return mc_bkg, mc_sig


# make folder
if not os.path.isdir("histograms"):
    os.makedirs("histograms")

fit_results_dir = "/global/cfs/cdirs/atlas/wcharm/trex_frag/charm_frag_6/wplusd_madgraph"

# get correlation matrix
res_file = ROOT.TFile(os.path.join(fit_results_dir, "Fits", os.path.basename(fit_results_dir) + ".root"), "READ")
result = res_file.Get("nll_simPdf_newasimovData_with_constr")
nuisance_parameters = []

print("--- list of NPs ---")
for x in result.floatParsFinal():
    # ignore gamma parameters because they are only
    # relevant for the mass fit, and not the zT shape
    if "gamma" in x.GetName():
        continue
    print(f"{x.GetName():20s} {x.getVal():+.4f} +/- {x.getError():+.4f}")
    nuisance_parameters.append(x.GetName())

# # print correlations
# print("\n--- NP correlations ---")
# for np1 in nuisance_parameters:
#     for np2 in nuisance_parameters:
#         if np1 == np2:
#             continue
#         corr = result.correlation(np1, np2)
#         print(f"{np1:20s} {np2:20s} {corr:+.4f}")

# get post-fit histograms
file_os = ROOT.TFile(os.path.join(fit_results_dir, "Histograms/OS_0tag_Dplus_PR_Dmeson_track_jet_10_zt_unfold_postFit.root"), "READ")
file_ss = ROOT.TFile(os.path.join(fit_results_dir, "Histograms/SS_0tag_Dplus_PR_Dmeson_track_jet_10_zt_unfold_postFit.root"), "READ")
print(file_os, file_ss)

# nominal MC tot histograms
mc_bkg = file_os.Get("h_tot_postFit").Clone(f"mc_tot_nominal")
mc_bkg_ss = file_ss.Get("h_tot_postFit").Clone(f"mc_bkg_ss_nominal")
mc_bkg.Add(mc_bkg_ss, -1)

# nominal signal histograms
mc_sig = file_os.Get("h_MGPy8EG_FxFx_WplusD_Matched_postFit").Clone(f"mc_sig_nominal")
mc_sig_ss = file_ss.Get("h_MGPy8EG_FxFx_WplusD_Matched_postFit").Clone(f"mc_sig_ss_nominal")
mc_sig.Add(mc_sig_ss, -1)

# data - bkg histogram
file_os_out = ROOT.TFile(os.path.join(fit_results_dir, "Histograms/OS_0tag_Dplus_PR_Dmeson_track_jet_10_zt_unfold_data.root"), "RECREATE")
h_data_OS = file_os.Get("h_Data").Clone("h_data_OS")
for bkg in ["MGPy8EG_FxFx_Wjets_Charm", "MGPy8EG_FxFx_Wjets_MisMatched", "MGPy8EG_FxFx_Wjets_Rest", "Top", "DibosonZjets_FxFx", "Multijet_MatrixMethod"]:
    h_bkg_OS = file_os.Get(f"h_{bkg}_postFit")
    h_data_OS.Add(h_bkg_OS, -1)
h_data_OS.Write()

file_ss_out = ROOT.TFile(os.path.join(fit_results_dir, "Histograms/SS_0tag_Dplus_PR_Dmeson_track_jet_10_zt_unfold_data.root"), "RECREATE")
h_data_SS = file_ss.Get("h_Data").Clone("h_data_SS")
for bkg in ["MGPy8EG_FxFx_Wjets_Charm", "MGPy8EG_FxFx_Wjets_MisMatched", "MGPy8EG_FxFx_Wjets_Rest", "Top", "DibosonZjets_FxFx", "Multijet_MatrixMethod"]:
    h_bkg_SS = file_ss.Get(f"h_{bkg}_postFit")
    h_data_SS.Add(h_bkg_SS, -1)
h_data_SS.Write()

# subtract signal from total MC to get background
mc_bkg.Add(mc_sig, -1)

# plotting style
mc_bkg.SetMarkerSize(0)
mc_sig.SetMarkerSize(0)

mc_bkg_ratio = divide_without_errors(mc_bkg, mc_bkg, f"{mc_bkg.GetName()}_ratio")
mc_sig_ratio = divide_without_errors(mc_sig, mc_sig, f"{mc_sig.GetName()}_ratio")

# make plots for all NPs
for np in nuisance_parameters:

    # fix the name
    np = np.replace("alpha_", "")

    # print
    print(f"Plotting {np}")

    # Get the variations
    mc_bkg_1up, mc_sig_1up = get_bkg_hist(file_os, file_ss, np, "Up")
    mc_bkg_1dn, mc_sig_1dn = get_bkg_hist(file_os, file_ss, np, "Down")

    # plot signal and background plots
    for hists, name in zip([[mc_bkg, mc_bkg_1up, mc_bkg_1dn, mc_bkg_ratio], [mc_sig, mc_sig_1up, mc_sig_1dn, mc_sig_ratio]], ["background", "signal"]):

        # upper panel
        hs1 = ROOT.THStack(f"hs1_{name}_{np}", f"hs1_{name}_{np}")
        hs1.Add(hists[0])
        hs1.Add(hists[1])
        hs1.Add(hists[2])

        # bottom panel
        mc_1up_ratio = divide_without_errors(hists[1], hists[0], f"{hists[1].GetName()}_ratio")
        mc_1dn_ratio = divide_without_errors(hists[2], hists[0], f"{hists[2].GetName()}_ratio")
        hs2 = ROOT.THStack(f"hs2_{name}_{np}", f"hs2_{name}_{np}")
        hs2.Add(hists[3])
        hs2.Add(mc_1up_ratio)
        hs2.Add(mc_1dn_ratio)

        # plot
        c, pad1, pad2 = createCanvasPads(f"mc_{name}_{np}")

        # top pad
        pad1.cd()
        hs1.Draw("HIST E1 NOSTACK")
        hs1.SetMinimum(1e-3)
        hs1.SetMaximum(1.5 * hists[0].GetMaximum())

        # legend
        leg = make_legend(3)
        leg.AddEntry(hists[0], "Nominal", "l")
        leg.AddEntry(hists[1], "1up", "l")
        leg.AddEntry(hists[2], "1dn", "l")
        leg.Draw()

        # atlas label
        atlas_label(["#sqrt{s} = 13 TeV, 140 fb^{-1}", f"{name}, track-jet R = 1.0", np], "Simulation Internal")

        # bottom pad
        pad2.cd()
        hs2.Draw("L NOSTACK")
        hs2.SetMinimum(0.51)
        hs2.SetMaximum(1.49)

        # configure
        configure_axis(hs1, hs2)
        hs1.GetYaxis().SetTitle("Entries")
        hs2.GetXaxis().SetTitle("z_{T}")
        hs2.GetYaxis().SetTitle("Ratio")

        # save
        c.Print(f"histograms/{name}_{np}.pdf")
