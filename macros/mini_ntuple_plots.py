#!/usr/bin/env python
import os
import ROOT
dirname = os.path.join(os.path.dirname(__file__), "../atlasrootstyle")
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetOptTitle(0)
ROOT.gROOT.LoadMacro(os.path.join(dirname, "AtlasStyle.C"))
ROOT.gROOT.LoadMacro(os.path.join(dirname, "AtlasLabels.C"))
ROOT.gROOT.LoadMacro(os.path.join(dirname, "AtlasUtils.C"))
ROOT.SetAtlasStyle()

# Enable multi-threading
ROOT.ROOT.EnableImplicitMT(8)

# invariant mass bins
BINS_DPLUS = (50, 1.7, 2.2)


def createCanvasPads(name):
    c = ROOT.TCanvas(name, name, 1200, 1200)
    # Upper histogram plot is pad1
    pad1 = ROOT.TPad(f"pad1_{name}", f"pad1_{name}", 0, 0.30, 1, 1.0)
    pad1.SetTopMargin(0.1)
    pad1.SetBottomMargin(0.05 / 0.70)
    pad1.SetFillStyle(4000)
    pad1.Draw()
    # Lower ratio plot is pad2
    c.cd()
    pad2 = ROOT.TPad(f"pad2_{name}", f"pad2_{name}", 0, 0.00, 1, 0.40)
    pad2.SetTopMargin(0.05 / 0.40)
    pad2.SetBottomMargin(0.10 / 0.40)
    pad2.SetFillStyle(4000)
    pad2.Draw()

    return c, pad1, pad2


def configure_axis(h_up, h_dn):
    GLOBAL_SF = 1.0
    h_up.GetYaxis().SetTitleSize(h_up.GetYaxis().GetTitleSize() * GLOBAL_SF)
    h_up.GetYaxis().SetTitleOffset(h_up.GetYaxis().GetTitleOffset() * (1 / (GLOBAL_SF * 1.1)))
    h_up.GetYaxis().SetLabelSize(h_up.GetYaxis().GetLabelSize() * GLOBAL_SF)
    h_up.GetXaxis().SetLabelSize(0)

    SF = 0.70 / 0.40
    h_dn.GetYaxis().SetTitleSize(h_dn.GetYaxis().GetTitleSize() * SF * GLOBAL_SF)
    h_dn.GetYaxis().SetTitleOffset(h_dn.GetYaxis().GetTitleOffset() * (1 / (GLOBAL_SF * 1.06 * SF)))
    h_dn.GetXaxis().SetTitleSize(h_dn.GetXaxis().GetTitleSize() * SF * GLOBAL_SF)
    h_dn.GetXaxis().SetTitleOffset(h_dn.GetXaxis().GetTitleOffset() * (1 / (GLOBAL_SF * 0.6 * SF)))
    h_dn.GetXaxis().SetLabelOffset(h_dn.GetXaxis().GetLabelOffset() * 4.0)
    h_dn.GetYaxis().SetLabelSize(h_dn.GetYaxis().GetLabelSize() * SF * GLOBAL_SF)
    h_dn.GetXaxis().SetLabelSize(h_dn.GetXaxis().GetLabelSize() * SF * GLOBAL_SF)

    # tick marks
    h_up.GetYaxis().SetNdivisions(506)
    h_dn.GetYaxis().SetNdivisions(306)


def atlas_label(labels=[], internal="Internal"):
    l1 = ROOT.TLatex()
    l1.SetTextFont(73)
    l1.SetTextSize(42)
    l1.DrawLatexNDC(0.18, 0.82, "ATLAS")
    l2 = ROOT.TLatex()
    l2.SetTextFont(43)
    l2.SetTextSize(42)
    l2.DrawLatexNDC(0.31, 0.82, internal)
    for i, lab in enumerate(labels):
        l2.DrawLatexNDC(0.18, 0.82 - (i + 1) * 0.05, lab)


def make_legend(N, x1=0.58, x2=0.92, y2=0.828, w=0.06, size=42):
    leg = ROOT.TLegend(x1, y2 - N * w, x2, y2)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg.SetFillStyle(0)
    leg.SetTextSize(size)
    leg.SetTextFont(43)
    return leg


def make_plots(args, h_backgr_OS, h_backgr_SS, h_signal_OS, h_signal_SS, label2):

    # remove signal from background
    h_backgr_OS.Add(h_signal_OS, -1)
    h_backgr_SS.Add(h_signal_SS, -1)

    # OS-SS subtraction
    h_signal_OS_SS = h_signal_OS.Clone(f"h_signal_OS_SS_{label2}")
    h_signal_OS_SS.Add(h_signal_SS, -1)
    h_backgr_OS_SS = h_backgr_OS.Clone(f"h_backgr_OS_SS_{label2}")
    h_backgr_OS_SS.Add(h_backgr_SS, -1)

    # OS, SS, OS-SS plots with loose cuts
    for hists, label in zip([[h_signal_OS, h_backgr_OS],
                             [h_signal_SS, h_backgr_SS],
                             [h_signal_OS_SS, h_backgr_OS_SS]],
                            ["OS", "SS", "OS-SS"]):

        # colors
        hists[0].SetFillColor(ROOT.kRed - 10)
        hists[1].SetFillColor(ROOT.kBlue - 10)

        # histogram stack
        hs = ROOT.THStack(f"hs_{label}_{label2}", f"hs_{label}_{label2}")
        hs.Add(hists[1])
        hs.Add(hists[0])

        # bottom panel
        h_ratio = hists[0].Clone(f"h_ratio_{label}_{label2}")
        h_ratio.SetFillColor(0)
        h_ratio.SetFillStyle(0)
        for i in range(1, h_ratio.GetNbinsX() + 1):
            s = hists[0].GetBinContent(i)
            b = hists[1].GetBinContent(i)
            if b != 0:
                h_ratio.SetBinContent(i, s / b)
                h_ratio.SetBinError(i, 0)
            else:
                h_ratio.SetBinContent(i, 0)
                h_ratio.SetBinError(i, 0)

        # plot
        c, pad1, pad2 = createCanvasPads(f"Dmeson_m_{label}_{label2}")

        # top pad
        pad1.cd()
        hs.Draw("hist")
        hs.SetMinimum(1e-3)
        hs.SetMaximum(1.5 * hs.GetMaximum())

        # legend
        leg = make_legend(2)
        leg.AddEntry(hists[0], "Signal", "f")
        leg.AddEntry(hists[1], "Background", "f")
        leg.Draw()

        # atlas label
        atlas_label([f"W+D(#rightarrowK#pi#pi), {label}", "#sqrt{s} = 13 TeV, 140 fb^{-1}", args.sample, f"{label2} cuts"], "Simulation Internal")

        # bottom pad
        pad2.cd()
        h_ratio.Draw("l")

        # configure
        configure_axis(hs, h_ratio)
        hs.GetYaxis().SetTitle("Events / (10 MeV)")
        h_ratio.GetXaxis().SetTitle("m(D^{#pm}) [GeV]")
        h_ratio.GetYaxis().SetTitle("S / B")

        # save
        c.Print(f"{args.output}/Dmeson_m_{label}_{label2}.pdf")

    # return OS-SS signal
    return h_signal_OS_SS


def main(args):

    # read input file
    import glob
    file_names = glob.glob(args.input)
    print(file_names)

    # get RDataFrame
    df = ROOT.RDataFrame("MiniCharm", file_names)

    # create histograms
    # D+ invariant mass from 1.7 to 2.2 GeV
    h_backgr_OS = df.Filter("channel.find(\"SR_2tag_Dplus_OS\")!=std::string::npos")
    h_backgr_SS = df.Filter("channel.find(\"SR_2tag_Dplus_SS\")!=std::string::npos")

    # filter out only the signal component
    h_signal_OS = h_backgr_OS.Filter("channel.find(\"SR_2tag_Dplus_OS_Matched\")!=std::string::npos")
    h_signal_SS = h_backgr_SS.Filter("channel.find(\"SR_2tag_Dplus_SS_Matched\")!=std::string::npos")

    # get histograms
    h_backgr_OS_loose = h_backgr_OS.Histo1D(("h_backgr_OS_loose", "h_backgr_OS_loose", *BINS_DPLUS), "Dmeson_m", "weight").GetValue()
    h_backgr_SS_loose = h_backgr_SS.Histo1D(("h_backgr_SS_loose", "h_backgr_SS_loose", *BINS_DPLUS), "Dmeson_m", "weight").GetValue()
    h_signal_OS_loose = h_signal_OS.Histo1D(("h_signal_OS_loose", "h_signal_OS_loose", *BINS_DPLUS), "Dmeson_m", "weight").GetValue()
    h_signal_SS_loose = h_signal_SS.Histo1D(("h_signal_SS_loose", "h_signal_SS_loose", *BINS_DPLUS), "Dmeson_m", "weight").GetValue()

    # make loose plots
    h_signal_OS_SS_loose = make_plots(args, h_backgr_OS_loose, h_backgr_SS_loose, h_signal_OS_loose, h_signal_SS_loose, "Loose")

    # final cuts
    dfs = [h_backgr_OS, h_backgr_SS, h_signal_OS, h_signal_SS]
    for i in range(len(dfs)):
        dfs[i] = dfs[i].Filter("Dmeson_pt > 8.0")
        dfs[i] = dfs[i].Filter("Dmeson_Chi2 < 8.0")
        dfs[i] = dfs[i].Filter("Dmeson_m > 1.8")
        dfs[i] = dfs[i].Filter("Dmeson_m <= 1.95")
        dfs[i] = dfs[i].Filter("Dmeson_costhetastar > -0.8")
        dfs[i] = dfs[i].Filter("Dmeson_track_jet_10_pt > 20.0")
        dfs[i] = dfs[i].Filter("fabs(Dmeson_track_jet_10_eta) < 1.5")
        dfs[i] = dfs[i].Filter("Dmeson_track_jet_10_zt < 1.0")
        dfs[i] = dfs[i].Filter("Dmeson_trk1_pt > 0.8")
        dfs[i] = dfs[i].Filter("Dmeson_trk2_pt > 0.8")
        dfs[i] = dfs[i].Filter("Dmeson_trk3_pt > 0.8")
        dfs[i] = dfs[i].Filter("Dmeson_Lxy > 1.1 + 0.028 * Dmeson_pt")
        dfs[i] = dfs[i].Filter("Dmeson_maxdR < (9.81961 / Dmeson_pt) - 40.0521 / (Dmeson_pt * Dmeson_pt)")
        dfs[i] = dfs[i].Filter("fabs(Dmeson_Impact) < 0.5")
        dfs[i] = dfs[i].Filter("fabs(Dmeson_ImpactSig) < 4.0")
        dfs[i] = dfs[i].Filter("(Dmeson_m-Dmeson_mKpi1) > 0.160")
        dfs[i] = dfs[i].Filter("(Dmeson_m-Dmeson_mKpi2) > 0.160")

    # get histograms
    h_backgr_OS_tight = dfs[0].Histo1D(("h_backgr_OS_tight", "h_backgr_OS_tight", *BINS_DPLUS), "Dmeson_m", "weight").GetValue()
    h_backgr_SS_tight = dfs[1].Histo1D(("h_backgr_SS_tight", "h_backgr_SS_tight", *BINS_DPLUS), "Dmeson_m", "weight").GetValue()
    h_signal_OS_tight = dfs[2].Histo1D(("h_signal_OS_tight", "h_signal_OS_tight", *BINS_DPLUS), "Dmeson_m", "weight").GetValue()
    h_signal_SS_tight = dfs[3].Histo1D(("h_signal_SS_tight", "h_signal_SS_tight", *BINS_DPLUS), "Dmeson_m", "weight").GetValue()

    # make tight plots
    print(f"h_signal_OS_tight: {h_signal_OS_tight.GetEntries()}")
    print(f"h_signal_SS_tight: {h_signal_SS_tight.GetEntries()}")
    print(f"h_signal_OS_tight: {h_signal_OS_tight.Integral()}")
    print(f"h_signal_SS_tight: {h_signal_SS_tight.Integral()}")
    h_signal_OS_SS_tight = make_plots(args, h_backgr_OS_tight, h_backgr_SS_tight, h_signal_OS_tight, h_signal_SS_tight, "Tight")

    # print summary
    print(f"Number of signal events Loose: {h_signal_OS_SS_loose.Integral()}")
    print(f"Number of signal events Tight: {h_signal_OS_SS_tight.Integral()}")


if __name__ == "__main__":

    # input arguments
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", help="input file(s) for glob", required=True)
    parser.add_argument("-s", "--sample", help="sample label (e.g. 'MGPy8EG (FxFx) W+jets' or 'Sherpa2.2.11 W+jets')", default="MGPy8EG (FxFx) W+jets")
    parser.add_argument("-o", "--output", help="output folder", default="plots")

    # parse arguments
    args = parser.parse_args()

    # make output dir
    if not os.path.isdir(args.output):
        os.makedirs(args.output)

    # run
    main(args)
